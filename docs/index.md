vimix performs **graphical mixing and blending** of several movie clips and
computer generated graphics, with image processing effects in real-time.

Its intuitive and hands-on user interface gives direct control on image opacity and
shape for producing live graphics during concerts and VJ-ing sessions.

The output image is typically projected full-screen on an external
monitor or a projector, but can be streamed live (SRT) or recorded (no audio).

![screenshot](vimix_0.2_beta.jpg)

Check the [Graphical User Manual](https://github.com/brunoherbelin/vimix/wiki/User-manual) or [demo videos](https://vimeo.com/vimix) to discover vimix.
Watch this selection of [videos by Jean Detheux](https://vimeo.com/showcase/7871359) to see what vimix can do.

## Install vimix

[Quick installation guide](https://github.com/brunoherbelin/vimix/wiki/Quick-Installation-Guide)

### Linux

[![Get it from flathub](https://flathub.org/assets/badges/flathub-badge-en.svg)](https://flathub.org/apps/details/io.github.brunoherbelin.Vimix)

### Mac OSX

Download package from [Github Releases](https://github.com/brunoherbelin/vimix/releases) page.

### Windows

*Please consider helping by compiling & providing packages for Windows.*

## Control vimix with OSC

You can control remotely vimix with [OSC](https://en.wikipedia.org/wiki/Open_Sound_Control), using [TouchOSC Mk1](https://github.com/brunoherbelin/vimix/wiki/TouchOSC-companion)
or using the [vimix OSC API](https://github.com/brunoherbelin/vimix/wiki/Open-Sound-Control-API) from your OSC applications.

## About

vimix is free and open source (GPL3+).

vimix is the successor of [GLMixer](https://sourceforge.net/projects/glmixer/), benefiting
from 10 years of refinement of User-Experience design since its [first draft](https://sourceforge.net/p/glmixer/wiki/GLMixer%20History/).

vimix is in its early infancy, open to [feature requests and bug reports](https://github.com/brunoherbelin/vimix/issues).

vimix welcomes contributions and support: check the [wiki](https://github.com/brunoherbelin/vimix/wiki) for more info.




[comment]: # (webpage hosted at https://brunoherbelin.github.io/vimix/)
